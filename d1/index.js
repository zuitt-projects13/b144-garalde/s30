const express = require("express")
// Mongoose is a package that allows creation of schemas to model our data structure and to have an access to a number of methods for manipulating our database
// -- to use the mongoDB codes like insertMany
const mongoose =require("mongoose")

const app = express()
const port = 3001



// mongoDB connection
// connectig mongoDB atlas
// change password and db name
mongoose.connect("mongodb+srv://hazelg21:hlg10212016@wdc028-course-booking.x4c4d.mongodb.net/batch144-to-do?retryWrites=true&w=majority",{
	useNewUrlParser: true,
	useUnifiedTopology:true
})

// to check if mongoDB connection is successful
let db = mongoose.connection;
// if error occured, output in the console
db.on("error", console.error.bind(console,"connection error"))
// but if the connection is successful, output in the console
db.once("open",() => console.log("We're connected to the cloud database."))

// end of mongoose connection code



app.use(express.json())
app.use(express.urlencoded({extended:true}))


// mongoose schema
// schema determines the structure of the documents to be written in the database. it acts as a blueprint to our data

// we will use Schema() constructor of the mongoose module to create a new Schema object
const taskSchema = new mongoose.Schema({
	name : String,
	status: {
		type: String,
		// default values are the predefined values for a field if we dont put any value
		default : "pending"
	}
})


// Model - to allow us to gain access in the schema and methods to perform CRUD to the database 
// when creating MODEL name use capital letter
// firstparameter of the mongoose model method indicates the collection in where to strore the data
// second parameter is used to specify the Schema/blueprint
const Task = mongoose.model("Task", taskSchema)


// Routes
// create a new task

/*
Business logic
1. Add a functionality to check if there are duplicate tasks
-if the task already exist, return there is a duplicate
-if the task doesn't exist, we can add it in the database
2. the tas data will be coming from the request's body
3. create new task OBJECT with properties we need
4. save the data

*/

app.post("/tasks", (request,response) => {
	// check if there are duplicate tasks
	// findOne() is a mongoose method that acts similar to "find"
	// it returns the first document that matches the search criteria
	// arrow function is for error handling
	Task.findOne({name: request.body.name}, (error,result) => {
		// if a document was found and the document name matches the information sent via client/postman
		if(result !== null && result.name == request.body.name){
			return response.send("Duplicate task found")
		}
		else {
			// NO DUPLICATE FOUND
			// create a task and save it to database
			let newTask = new Task ({
				name: request.body.name
			})

			// arrow function is for error handling
			// error,successful
			newTask.save((saveErr,savedTask) => {
				// if there are errors in saving
				if (saveErr){
					// this will print any error in saving
					// saveErr is an error object that wil contain detaols about the error
					// Errors normally comes as an object data type
					return console.error(saveErr)
				}
				else {
					// no error found while creating the document
					// return a status code of 201 for successfully created
					return response.status(201).send("New Task Created")
				}
			})

		}	
	})

})


/*
Business logic for retrieving all data
1. retrieve all data using find() method
2. if an error is encountered, print the error
3. if no error is found send success status back to the client and return an array of documents

*/


app.get("/tasks",(request,response) => {
	// an empty {} means it returns all the documents and store them in the "result" parameter of the call back function
	Task.find({},(error,result) => {
		if (error){
			return console.log(error)
		}
		else {
			return response.status(200).json(
				{data: result}
			)
		}
	})
})


/*
Activity:
1. Create a User schema.
2. Create a User model.
3. Create a POST route that will access the "/signup" route that will create a user.
4. Process a POST request at the "/signup" route using postman to register a user.
5. Create a GET route that will return all users.
6. Process a GET request at the "/users" route using postman.
7. Create a git repository named S30.
8. Initialize a local git repository, add the remote link and push to git with the commit message of s30 Activity.
9. Add the link in Boodle named Express js Data Persistence via Mongoose ODM.


*/

/*
Register a user
1. check for duplicate
	-if user already exist, we return an error
	-if user doesn't exist, we add it in the database
		-if the username and password are both not blank
			-if blank, send response "BOTH username and password be provided"
			-both condition has been met, create a new object
			-save the new object
				-error handling for saving, if error return an error message
				-return 201 status for creation and "New User Registered"

2. 
*/



const userSchema = new mongoose.Schema({
	userName :  {
		type: String,
		default : null
	},
	password : {
		type: String,
		default : null
	}
})


const User = mongoose.model("User", userSchema)


app.post("/signup",(request,response) => {
	User.findOne({userName : request.body.userName},(error,result) => {
		if(result !== null && request.body.userName == result.userName){
			return response.send("User is already registered.")
		}
		else if (request.body.userName == "" || request.body.password == "" || request.body.userName == null || request.body.password == null){
			return response.send("BOTH username and password be provided.")
		}	
		else {
			let newUser = new User ({
				userName : request.body.userName,
				password : request.body.password
			})
			
			newUser.save((saveErr,savedUser) => {
				if (saveErr){
					return console.error(saveErr)
				}
				else {
					return response.status(201).send("Successfully added user")
				}
			})
			
		}
	})
})


app.get("/users",(request,response) => {
	User.find({}, (error,result) => {
		if(error){
			return console.error(error)
		}
		else {
			return response.status(200).json({result})
		}
	})
})



app.listen(port, () => console.log(`Server is running at port: ${port}`))